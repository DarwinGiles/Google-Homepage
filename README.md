# Google Homepage Clone

This is a frontend-only copy of [Google's homepage.](https://www.google.com/) It has none of the actual functionality of Google.

This project was created while following The Odin Project's [curriculum](http://www.theodinproject.com/courses/web-development-101/lessons/html-css).